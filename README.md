# PTT Kargo Takip APIlari

(English below)

PTT sisteminde kargo aramak icin PTT Mobil'in kullandigi APIlari kullanan ornek bir script ve API dokumanlari.

API dokumanlarina [buradan](https://gitlab.com/a/PTT-API/blob/master/APIDocs.md) erisebilirsiniz.

Ornek scripti python3.6 ile calistirabilirsiniz.

**Onemli Bilgi:** PTT ile bir baglantim yok, sadece API servislerini bir suredir kendime bildirim vs gonderme sebebiyle takip ediyorum ve bu APIlari dokumante etmek istedim.

---

# PTT Shipment Tracking APIs

(Turkce yukarida)

PTT is the Turkish National Postal Service (they also do a couple other things but no one cares, really), similar to USPS.

This repo contains a sample script and API documentation about PTT's APIs used by their mobile app.

You may access the API documents [here](https://gitlab.com/a/PTT-API/blob/master/APIDocs.md).

You may run the sample script with python3.6+.

**Important Notice:** I am not affiliated with PTT, I just have been using their API services to send myself notifications about my shipments and wanted to document them.
